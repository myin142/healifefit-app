import { ButtonInputComponent } from './button-input.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('ButtonInputComponent', () => {

    let fixture: ComponentFixture<ButtonInputComponent>;
    let component: ButtonInputComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ButtonInputComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });

        fixture = TestBed.createComponent(ButtonInputComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('Create Button', () => {

        let button: DebugElement;

        beforeEach(() => {
            component.name = 'Button Name';
            component.showInput = false;
            fixture.detectChanges();

            button = fixture.debugElement.query(By.css('ion-button'));
        });

        it('should contain button with name', () => {
            expect(button.nativeElement.textContent).toContain(component.name);
        });

        it('should not contain button with name after click', () => {
            button.nativeElement.click();
            fixture.detectChanges();

            const btn = fixture.nativeElement.querySelector('ion-button');
            expect(btn).toBeFalsy();
        });

    });

    describe('Submit', () => {

        it('should emit value', () => {
            spyOn(component.enter, 'emit');
            const value = 'Value';
            component.submit(value);

            expect(component.enter.emit).toHaveBeenCalledWith(value);
        });

        it('should hide input', () => {
            component.showInput = true;
            component.submit('');

            expect(component.showInput).toBeFalsy();
        });

    });

});
