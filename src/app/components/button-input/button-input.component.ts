import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-button-input',
    templateUrl: './button-input.component.html',
})
export class ButtonInputComponent {
    @Input() name: string;
    @Output() enter = new EventEmitter<string>();

    showInput = false;

    submit(value: string) {
        this.enter.emit(value);
        this.showInput = false;
    }
}
