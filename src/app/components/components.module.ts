import { NgModule } from '@angular/core';
import { ButtonInputComponent } from './button-input/button-input.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { InputComponent } from './input/input.component';
import { RangeInputComponent } from './range-input/range-input.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        ReactiveFormsModule,
    ],
    declarations: [
        ButtonInputComponent,
        InputComponent,
        RangeInputComponent,
    ],
    exports: [
        ButtonInputComponent,
        InputComponent,
        RangeInputComponent,
    ],
})
export class ComponentsModule {}
