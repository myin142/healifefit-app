import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
})
export class InputComponent {
    @Input() type = 'text';
    @Input() placeholder = '';
    @Input() value = '';
    @Input() resetInput = true;

    @Output() enter = new EventEmitter<string>();
    @Output() close = new EventEmitter<any>();

    control = new FormControl();

    emitValue() {
        this.enter.emit(this.control.value);
        this.closeInput();
    }

    closeInput() {
        if (this.resetInput) {
            this.control.reset();
        }

        this.close.emit();
    }
}
