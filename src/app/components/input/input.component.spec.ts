import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { InputComponent } from './input.component';
import { By } from '@angular/platform-browser';

describe('InputComponent', () => {

    let fixture: ComponentFixture<InputComponent>;
    let component: InputComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [InputComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });

        fixture = TestBed.createComponent(InputComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('Input', () => {

        let input: DebugElement;

        beforeEach(() => {
            input = fixture.debugElement.query(By.css('ion-input'));
        });

        it('should contain ionic input', () => {
            expect(input).toBeTruthy();
        });

    });

    describe('Emit Value', () => {

        it('should emit enter event with control value', () => {
            spyOn(component.enter, 'emit');

            const value = 'Value';
            component.control.setValue(value);
            component.emitValue();

            expect(component.enter.emit).toHaveBeenCalledWith(value);
        });

        it('should reset control value if reset enabled', () => {
            component.control.setValue('Value');
            component.resetInput = true;
            component.emitValue();

            expect(component.control.value).toBeFalsy();
        });

        it('should emit close', () => {
            spyOn(component.close, 'emit');
            component.emitValue();

            expect(component.close.emit).toHaveBeenCalled();
        });

    });

});
