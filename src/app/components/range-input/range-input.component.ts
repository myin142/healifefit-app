import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-range-input',
    templateUrl: './range-input.component.html',
})
export class RangeInputComponent {
    @Input() value: number;
    @Input() placeholder: string;
    @Input() label: string;

    @Output() enter = new EventEmitter<number>();

    constructor() {}

    emitValue(value: any) {
        if (value !== '') {
            this.enter.emit(value);
        }
    }

}
