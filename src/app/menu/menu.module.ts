import { NgModule } from '@angular/core';
import { MenuListComponent } from './menu-list/menu-list.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MenuHeaderComponent } from './menu-header/menu-header.component';
import { MenuBackHeaderComponent } from './menu-back-header/menu-back-header.component';
import { MenuCloseHeaderComponent } from './menu-close-header/menu-close-header.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        RouterModule,
    ],
    declarations: [
        MenuListComponent,
        MenuHeaderComponent,
        MenuBackHeaderComponent,
        MenuCloseHeaderComponent,
    ],
    exports: [
        MenuListComponent,
        MenuHeaderComponent,
        MenuBackHeaderComponent,
        MenuCloseHeaderComponent,
    ],
})
export class MenuModule {}
