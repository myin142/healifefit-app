import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { MenuListComponent } from './menu-list.component';
import { MenuItem } from './menu-item.model';
import { IonicModule, MenuController } from '@ionic/angular';

describe('MenuListComponent', () => {

    let fixture: ComponentFixture<MenuListComponent>;
    let component: MenuListComponent;

    let menu: MenuController;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                MenuListComponent,
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                IonicModule,
                RouterTestingModule.withRoutes([
                    { path: 'item1', component: MenuListComponent },
                    { path: 'item2', component: MenuListComponent },
                ]),
            ],
        }).compileComponents();

        fixture = TestBed.createComponent(MenuListComponent);
        component = fixture.componentInstance;

        menu = TestBed.get(MenuController);
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('Menu Items', () => {

        let app: any;

        const items: MenuItem[] = [
            {name: 'Item 1', link: '/item1', icon: 'icon1'},
            {name: 'Item 2', link: '/item2', icon: 'icon2'},
        ];

        beforeEach(() => {
            component.menuItems = items;
            fixture.detectChanges();
            app = fixture.nativeElement;
        });

        it('should have labels', () => {
            const menuItems = app.querySelectorAll('ion-label');
            expect(menuItems.length).toEqual(component.menuItems.length);
            component.menuItems.forEach((value, index) => {
                expect(menuItems[index].textContent).toContain(value.name);
            });
        });

        it('should have links', () => {
            const menuItems = app.querySelectorAll('ion-item');
            expect(menuItems.length).toBeGreaterThanOrEqual(component.menuItems.length);
            component.menuItems.forEach((value, index) => {
                expect(menuItems[index].getAttribute('ng-reflect-router-link')).toEqual(value.link);
            });
        });

        it('should have icons', () => {
            const icons = app.querySelectorAll('ion-icon');
            component.menuItems.forEach((value , index) => {
                expect(icons[index].getAttribute('ng-reflect-name')).toEqual(value.icon);
            });
        });

        it('should default to arrow-dropright icon', () => {
            component.menuItems = [...items, {name: 'Item 3', link: '/item3'}];
            fixture.detectChanges();

            const icons = app.querySelectorAll('ion-icon');
            expect(icons[icons.length - 1].getAttribute('ng-reflect-name')).toEqual('arrow-dropright');
        });

        it('should close menu on click', () => {
            spyOn(menu, 'close');

            const menuItem = app.querySelector('ion-item');
            menuItem.click();

            expect(menu.close).toHaveBeenCalled();
        });

    });

});
