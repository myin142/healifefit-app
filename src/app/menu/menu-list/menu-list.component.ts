import { Component, Input } from '@angular/core';
import { MenuItem } from './menu-item.model';
import { MenuController } from '@ionic/angular';

@Component({
    selector: 'app-menu-list',
    templateUrl: './menu-list.component.html',
})
export class MenuListComponent {
    @Input() menuItems: MenuItem[];

    constructor(private menu: MenuController) {}

    closeMenu() {
        this.menu.close();
    }
}
