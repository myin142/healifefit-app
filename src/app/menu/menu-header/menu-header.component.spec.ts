import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuHeaderComponent } from './menu-header.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('MenuHeaderComponent', () => {

    let fixture: ComponentFixture<MenuHeaderComponent>;
    let component: MenuHeaderComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MenuHeaderComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });

        fixture = TestBed.createComponent(MenuHeaderComponent);
        component = fixture.componentInstance;
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should contain title', () => {
        const title = 'Title';
        component.title = title;
        fixture.detectChanges();

        const elem = fixture.nativeElement.querySelector('ion-title');
        expect(elem.textContent).toEqual(title);
    });

    it('should have menu button', () => {
        const elem = fixture.nativeElement.querySelector('ion-menu-button');
        expect(elem).toBeDefined();
    });

});
