import { Component } from '@angular/core';
import { MenuItem } from '../menu/menu-list/menu-item.model';

@Component({
    selector: 'app-time-base',
    template: '<app-menu-header title="CaliBase"></app-menu-header>' +
        '<ion-content><app-menu-list [menuItems]="menuItems"></app-menu-list></ion-content>',
})
export class CaliBasePage {

    menuItems: MenuItem[] = [
        {name: 'Track Workout', link: 'track'},
        {name: 'Workout History', link: 'history'},
    ];

}
