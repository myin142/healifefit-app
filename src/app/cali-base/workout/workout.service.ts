import { Injectable } from '@angular/core';
import { CrudStorageService } from '../../service/crud-storage.service';
import { Workout } from './workout.model';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';

@Injectable()
export class WorkoutService extends CrudStorageService<Workout> {

    currentWorkout = new BehaviorSubject<Workout>(null);
    savedList = new BehaviorSubject<Workout[]>([]);

    constructor(storage: Storage) {
        super(storage, 'workout-service-key');
    }

    newWorkout(name: string) {
        this.currentWorkout.next({
            name,
            rounds: [],
        });
    }

    async saveList(list: Workout[]): Promise<void> {
        list = list.sort((x1, x2) => moment(x2.finishDate).diff(x1.finishDate));
        super.saveList(list);
    }

    addItem(item: Workout) {
        if (this.listValue.length >= 10) {
            this.listValue.pop();
        }

        super.addItem(item);
    }

    get listValue() {
        return this.savedList.getValue();
    }

}
