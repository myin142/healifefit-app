import { WorkoutService } from './workout.service';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { StorageMockProvider } from '../../test/native-storage.mock';
import { Workout } from './workout.model';
import { componentFactoryName } from '@angular/compiler';

describe('WorkoutService', () => {

    let service: WorkoutService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                WorkoutService,
                StorageMockProvider,
            ],
        });

        service = TestBed.get(WorkoutService);
    });

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    it('should remove oldest workout if length past 10 items', fakeAsync(() => {
        const oldest: Workout = { name: 'Oldest', rounds: [], finishDate: newDateAddMonths(-10) };
        const mockList: Workout[] = new Array(9).fill({ name: '', rounds: [], finishDate: newDateAddMonths(-1) });
        mockList.push(oldest);

        service.saveList(mockList);
        tick();

        const newest: Workout = { name: 'Newest', rounds: [] };
        service.addItem(newest);
        tick();

        const list = service.savedList.getValue();
        expect(list).not.toContain(oldest);
        expect(list).toContain(newest);
        expect(list.length).toEqual(10);
    }));

    it('should saveList sorted by newest finishDate', fakeAsync(() => {
        const workouts: Workout[] = [
            {
                name: 'Workout Past',
                rounds: [],
                finishDate: newDateAddMonths(-1),
            },
            {
                name: 'Workout Now',
                rounds: [],
                finishDate: new Date(),
            },
            {
                name: 'Workout Future',
                rounds: [],
                finishDate: newDateAddMonths(1),
            },
        ];

        service.saveList([ ...workouts ]);
        tick();

        expect(service.savedList.value).toEqual([
            workouts[2], workouts[1], workouts[0],
        ]);
    }));

    describe('Current Workout', () => {

        it('should create new workout with name', () => {
            const name = 'Workout';
            service.newWorkout(name);

            expect(service.currentWorkout.value).toEqual(jasmine.objectContaining({
                name,
            }));
        });

    });

    function newDateAddMonths(months?: number): Date {
        const date = new Date();
        date.setMonth(date.getMonth() + (months || 0));
        return date;
    }

});
