export interface Workout {
    name: string;
    rounds: Round[];
    finishDate?: Date;
}

export interface Round {
    exercises: Exercise[];
    set: number;
}

interface Exercise {
    name: string;
    rep: number;
}
