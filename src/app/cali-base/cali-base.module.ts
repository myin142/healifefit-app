import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { CaliBasePage } from './cali-base.page';
import { MenuModule } from '../menu/menu.module';
import { WorkoutService } from './workout/workout.service';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        MenuModule,
        RouterModule.forChild([
            {path: '', component: CaliBasePage},
            {path: 'track', loadChildren: () => import('./track-workout/track-workout.module').then(m => m.TrackWorkoutModule)},
            {path: 'history', loadChildren: () => import('./workout-history/workout-history.module').then(m => m.WorkoutHistoryModule)},
        ]),
    ],
    declarations: [CaliBasePage],
    providers: [WorkoutService],
})
export class CaliBaseModule {
}
