import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { RangeInputComponent } from '../../components/range-input/range-input.component';

@Directive({
    selector: '[appNumberInput]',
})
export class NumberInputDirective {

    @Input('appNumberInput') value: number;
    @Input() label: string;
    @Output() numberChange = new EventEmitter<number>();

    constructor(private popover: PopoverController) {
        this.numberChange.subscribe(() => {
            this.dismissIfAvailable();
        });
    }

    private async dismissIfAvailable() {
        if (await this.popover.getTop()) {
            this.popover.dismiss();
        }
    }

    @HostListener('press', ['$event'])
    async openNumberRange(event) {
        const popoverRef = await this.popover.create({
            component: RangeInputComponent,
            componentProps: {
                label: this.label,
                placeholder: this.value,
                enter: this.numberChange,
            },
            event,
            translucent: true,
        });

        popoverRef.present();
    }

    @HostListener('click')
    increaseByOne() {
        this.numberChange.emit(Number(this.value) + 1);
    }

}
