import { TrackWorkoutPage } from './track-workout.page';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { WorkoutService } from '../workout/workout.service';
import { of } from 'rxjs';

describe('TrackWorkoutPage', () => {

    let fixture: ComponentFixture<TrackWorkoutPage>;
    let component: TrackWorkoutPage;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TrackWorkoutPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                {
                    provide: WorkoutService, useValue:
                        {
                            currentWorkout: of(),
                        },
                },
            ],
        });

        fixture = TestBed.createComponent(TrackWorkoutPage);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

});
