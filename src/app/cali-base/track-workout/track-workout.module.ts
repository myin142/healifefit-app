import { NgModule } from '@angular/core';
import { TrackWorkoutPage } from './track-workout.page';
import { MenuModule } from '../../menu/menu.module';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../../components/components.module';
import { RangeInputComponent } from '../../components/range-input/range-input.component';
import { NumberInputDirective } from './number-input.directive';

@NgModule({
    imports: [
        MenuModule,
        IonicModule,
        RouterModule.forChild([
            {path: '', component: TrackWorkoutPage},
        ]),
        CommonModule,
        ComponentsModule,
    ],
    declarations: [
        TrackWorkoutPage,
        NumberInputDirective,
    ],
    entryComponents: [
        RangeInputComponent,
    ],
})
export class TrackWorkoutModule {}
