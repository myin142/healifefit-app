import { Component } from '@angular/core';
import { WorkoutService } from '../workout/workout.service';
import { Round, Workout } from '../workout/workout.model';
import { AlertController } from '@ionic/angular';

@Component({
    selector: 'app-track-workout',
    templateUrl: './track-workout.page.html',
})
export class TrackWorkoutPage {

    workout: Workout;

    constructor(private service: WorkoutService,
                private alert: AlertController) {

        this.service.currentWorkout.subscribe(workout => {
            this.workout = workout;
        });
    }

    createWorkout(workoutName: string): void {
        this.service.newWorkout(workoutName);
    }

    createRound(): void {
        this.workout.rounds.push({
            exercises: [],
            set: 0,
        });
    }

    createExercise(exerciseName: string, round: Round): void {
        round.exercises.push({
            name: exerciseName,
            rep: 0,
        });
    }

    async finishWorkout() {
        const alertRef = await this.alert.create({
            header: 'Finish Workout',
            message:
                `Are you sure you want to finish your workout?
                You cannot change it afterwards.`,
            buttons: [
                {
                    text: 'Yes',
                    handler: this.saveWorkout.bind(this),
                },
                {
                    text: 'No',
                },
            ],
        });

        await alertRef.present();
    }

    private saveWorkout() {
        this.workout.finishDate = new Date();
        this.service.addItem(this.workout);
        this.service.currentWorkout.next(null);
    }

}
