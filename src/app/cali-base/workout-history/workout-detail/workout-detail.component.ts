import { Component, Input } from '@angular/core';
import { Workout } from '../../workout/workout.model';

@Component({
    selector: 'app-workout-detail',
    templateUrl: './workout-detail.component.html',
})
export class WorkoutDetailComponent {
    @Input() workout: Workout;
}
