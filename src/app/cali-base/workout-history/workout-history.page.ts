import { Component, OnInit } from '@angular/core';
import { WorkoutService } from '../workout/workout.service';
import { Workout } from '../workout/workout.model';
import { ModalController } from '@ionic/angular';
import { WorkoutDetailComponent } from './workout-detail/workout-detail.component';

@Component({
    selector: 'app-workout-history',
    templateUrl: './workout-history.page.html',
})
export class WorkoutHistoryPage implements OnInit {

    workoutHistory: Workout[];

    constructor(private workout: WorkoutService,
                private modal: ModalController) {}

    ngOnInit() {
        this.workout.savedList.subscribe(list => {
            this.workoutHistory = list;
        });
    }

    async showWorkoutDetails(workout: Workout) {
        const modalRef = await this.modal.create({
            component: WorkoutDetailComponent,
            componentProps: { workout },
        });

        modalRef.present();
    }
}
