import { NgModule } from '@angular/core';
import { WorkoutHistoryPage } from './workout-history.page';
import { RouterModule } from '@angular/router';
import { MenuModule } from '../../menu/menu.module';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { WorkoutDetailComponent } from './workout-detail/workout-detail.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {path: '', component: WorkoutHistoryPage},
        ]),
        MenuModule,
        IonicModule,
        CommonModule,
    ],
    declarations: [
        WorkoutHistoryPage,
        WorkoutDetailComponent,
    ],
    entryComponents: [
        WorkoutDetailComponent,
    ],
})
export class WorkoutHistoryModule {}
