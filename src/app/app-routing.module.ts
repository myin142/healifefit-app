import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'timebase', pathMatch: 'full'},
    {path: 'timebase', loadChildren: () => import('./time-base/time-base.module').then(m => m.TimeBaseModule)},
    {path: 'calibase', loadChildren: () => import('./cali-base/cali-base.module').then(m => m.CaliBaseModule)},
    {path: 'foodbase', loadChildren: () => import('./food-base/food-base.module').then(m => m.FoodBaseModule)},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
