import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SearchPage } from './search.page';
import { FoodReportComponent } from './food-report/food-report.component';
import { FoodApiService } from '../service/food-api.service';
import { HttpClientModule } from '@angular/common/http';
import { MenuModule } from '../../menu/menu.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([{path: '', component: SearchPage}]),
        HttpClientModule,
        MenuModule,
    ],
    providers: [
        FoodApiService,
    ],
    declarations: [
        SearchPage,
        FoodReportComponent,
    ],
    entryComponents: [
        FoodReportComponent,
    ],
})
export class SearchModule {
}
