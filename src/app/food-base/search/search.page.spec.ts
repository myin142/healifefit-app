import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { SearchPage } from './search.page';
import { EMPTY, NEVER, of } from 'rxjs';
import { IonicModule, ModalController } from '@ionic/angular';
import { FoodApiService } from '../service/food-api.service';
import { FoodField } from '../service/food-models';
import { FoodReportComponent } from './food-report/food-report.component';
import { ModalControllerMockProvider } from '../../test/modal-controller.mock';
import { By } from '@angular/platform-browser';

describe('SearchPage', () => {
    let component: SearchPage;
    let fixture: ComponentFixture<SearchPage>;

    let foodApi: FoodApiService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [IonicModule],
            declarations: [SearchPage],
            providers: [
                ModalControllerMockProvider,
                { provide: FoodApiService, useValue: new FoodApiService(null) },
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });

        foodApi = TestBed.get(FoodApiService);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('Search Food', () => {

        it('should search foods', () => {
            spyOn(foodApi, 'search').and.returnValue(NEVER);
            const text = 'text';

            component.searchFood(text);

            expect(foodApi.search).toHaveBeenCalledWith(text);
        });

        it('should save list to foodList', fakeAsync(() => {
            const resultList: Array<FoodField> = [{
                fdcId: 0,
                description: '',
            }];
            spyOn(foodApi, 'search').and.returnValue(of({ foods: resultList }));

            component.searchFood('');
            tick();

            expect(component.foodList).toEqual(resultList);
        }));

        it('should enable loading after calling search', () => {
            spyOn(foodApi, 'search').and.returnValue(NEVER);

            component.searchFood('');
            expect(component.loading).toBeTruthy();
        });

        it('should disable loading after search returns', fakeAsync(() => {
            spyOn(foodApi, 'search').and.returnValue(EMPTY);

            component.searchFood('');
            tick();

            expect(component.loading).toBeFalsy();
        }));

    });

    describe('Get Food', () => {

        it('should get food informations', () => {
            spyOn(foodApi, 'getFood').and.returnValue(NEVER);
            const id = 1;

            component.getFood(id);

            expect(foodApi.getFood).toHaveBeenCalledWith(id);
        });

        describe('Success', () => {

            let modal: ModalController;
            let result;

            beforeEach(() => {
                modal = TestBed.get(ModalController);
                result = { data: '' };

                spyOn(foodApi, 'getFood').and.returnValue(of(result));
                spyOn(modal, 'create').and.callThrough();
            });

            it('should create modal of FoodReportComponent', fakeAsync(() => {
                component.getFood(1);
                tick();

                expect(modal.create).toHaveBeenCalledWith(jasmine.objectContaining({
                    component: FoodReportComponent,
                }));
            }));

            it('should pass food result to component', fakeAsync(() => {
                component.getFood(1);
                tick();

                expect(modal.create).toHaveBeenCalledWith(jasmine.objectContaining({
                    componentProps: { food: result },
                }));
            }));

        });

        it('should enable loading after calling getFood', () => {
            spyOn(foodApi, 'getFood').and.returnValue(NEVER);

            component.getFood(0);
            expect(component.loading).toBeTruthy();
        });

        it('should disable loading after getFood returns', fakeAsync(() => {
            spyOn(foodApi, 'getFood').and.returnValue(EMPTY);

            component.getFood(0);
            tick();

            expect(component.loading).toBeFalsy();
        }));

    });

    describe('DOM', () => {

        describe('Progress Bar', () => {

            it('should show on loading', () => {
                component.loading = true;
                fixture.detectChanges();

                const progress = fixture.debugElement.query(By.css('ion-progress-bar'));
                expect(progress).toBeTruthy();
            });

            it('should hide on not loading', () => {
                component.loading = false;
                fixture.detectChanges();

                const progress = fixture.debugElement.query(By.css('ion-progress-bar'));
                expect(progress).toBeFalsy();
            });

        });

        describe('Food List', () => {

            let foods: FoodField[];

            beforeEach(() => {
                foods = [
                    { fdcId: 1, description: 'Food 1' },
                    { fdcId: 2, description: 'Food 2' },
                ];

                component.foodList = foods;
                fixture.detectChanges();
            });

            it('should list food list items', () => {
                const list = fixture.debugElement.queryAll(By.css('ion-list ion-item'));
                list.forEach((item, i) => {
                    expect(item.nativeElement.textContent).toContain(foods[i].description);
                });
            });

            it('should get food on click', () => {
                spyOn(component, 'getFood');

                const item = fixture.debugElement.query(By.css('ion-list ion-item'));
                item.nativeElement.click();

                expect(component.getFood).toHaveBeenCalled();
            });

        });

    });

});
