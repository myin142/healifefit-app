import { Component, OnInit } from '@angular/core';
import { FoodField } from '../service/food-models';
import { Observable } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { FoodReportComponent } from './food-report/food-report.component';
import { FoodApiService } from '../service/food-api.service';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-search',
    templateUrl: './search.page.html',
})
export class SearchPage {

    foodList: Array<FoodField>;
    loading = false;

    constructor(private food: FoodApiService,
                private modalController: ModalController) {}

    searchFood(text: string): void {
        this.serviceCall(this.food.search(text), result => {
            this.foodList = result.foods;
        });
    }

    getFood(id: number): void {
        this.serviceCall(this.food.getFood(id), async result => {
            const modal = await this.modalController.create({
                component: FoodReportComponent,
                componentProps: {food: result},
            });

            modal.present();
        });
    }

    private serviceCall<T>(observable: Observable<T>, success: (r: T) => void): void {
        this.loading = true;
        observable.subscribe(success, () => {
        }, () => {
            this.loading = false;
        });
    }

}
