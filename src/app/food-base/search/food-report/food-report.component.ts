import { Component, Input } from '@angular/core';
import { FoodReport } from '../../service/food-models';

@Component({
    selector: 'app-food-report',
    templateUrl: './food-report.component.html',
})
export class FoodReportComponent {
    @Input() food: FoodReport;
}
