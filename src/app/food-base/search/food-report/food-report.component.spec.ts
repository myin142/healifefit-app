import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FoodReportComponent } from './food-report.component';
import { FoodReport } from '../../service/food-models';
import { By } from '@angular/platform-browser';

describe('FoodReportComponent', () => {
    let component: FoodReportComponent;
    let fixture: ComponentFixture<FoodReportComponent>;

    let food: FoodReport;
    const defaultNutrient = { id: 0, name: '', number: '', unitName: 'g' };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FoodReportComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        });

        food = {
            fdcId: 10,
            description: 'Description',
            foodNutrients: [],
        };
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FoodReportComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be empty if food not defined', () => {
        component.food = undefined;
        fixture.detectChanges();

        expect(fixture.nativeElement.textContent).toEqual('');
    });

    it('should contain food description', () => {
        component.food = food;
        fixture.detectChanges();

        const row = fixture.debugElement.query(By.css('ion-row'));
        expect(row.nativeElement.textContent).toContain(food.description);
    });

    describe('Nutrients', () => {

        let nutrients: Array<any>;

        beforeEach(() => {
            nutrients = [
                { nutrient: { ...defaultNutrient, name: 'Nutrients' } },
                { nutrient: { ...defaultNutrient, name: 'Nutrient 1' }, amount: 1 },
                { nutrient: { ...defaultNutrient, name: 'Nutrient 2' }, amount: 2 },
            ];

            component.food = {
                ...food,
                foodNutrients: nutrients,
            };
            fixture.detectChanges();
        });

        it('should contain food nutrient names', () => {
            const rows = queryNutrientsRows();

            expect(rows.length).toEqual(nutrients.length);
            rows.forEach(({ nativeElement }, i) => {
                expect(nativeElement.textContent).toContain(nutrients[i].nutrient.name);
            });
        });

        it('should contain amount with unitName', () => {
            const rows = queryNutrientsRows();

            rows.forEach(({ nativeElement}, i ) => {
                const n = nutrients[i];

                if (n.amount) {
                    expect(nativeElement.textContent).toContain(n.amount + n.nutrient.unitName);
                }
            });
        });

        it('should use label for food nutrient without amount', () => {
            const rows = queryNutrientsRows('ion-label');

            expect(rows.length).toEqual(1);
            expect(rows[0].nativeElement.textContent).toEqual(nutrients[0].nutrient.name);
        });

        function queryNutrientsRows(nestedQuery?: string) {
            let rows = fixture.debugElement.queryAll(By.css('ion-row'));
            rows.shift();

            if (nestedQuery) {
                rows = rows.map(row => row.query(By.css(nestedQuery))).filter(x => x !== null);
            }

            return rows;
        }

    });

});
