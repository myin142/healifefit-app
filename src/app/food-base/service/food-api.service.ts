import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FoodReport, SearchResult } from './food-models';

@Injectable()
export class FoodApiService {

    private API_KEY = '8CDd1Sk5w1TAkwPKGlBTy8TSd1WRvQDpti0MGlxP';
    private BASE_URL = `https://api.nal.usda.gov/fdc/v1/`;

    constructor(private http: HttpClient) {
    }

    search(text: string): Observable<SearchResult> {
        const request: SearchRequest = {
            generalSearchInput: text,
            includeDataTypes: {
                Foundation: true,
                'SR Legacy': true,
            },
        };

        return this.http.post<SearchResult>(this.searchUrl, request);
    }

    getFood(id: number): Observable<FoodReport> {
        return this.http.get<FoodReport>(this.createUrl(`${id}`));
    }

    private get searchUrl(): string {
        return this.createUrl('search');
    }

    private createUrl(postfix: string): string {
        return `${this.BASE_URL}${postfix}?api_key=${this.API_KEY}`;
    }

}

interface SearchRequest {
    generalSearchInput?: string;
    includeDataTypes?: {
        [type in 'Survey (FNDDS)' | 'Foundation' | 'Branded' | 'SR Legacy']?: boolean
    };
    ingredients?: string;
    brandOwner?: string;
    requireAllWords?: string;
    pageNumber?: string;
    sortField?: string;
    sortDirection?: string;
}
