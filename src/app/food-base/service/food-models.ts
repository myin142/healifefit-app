export interface SearchResult {
    totalHits: number;
    currentPage: number;
    totalPages: number;
    foods: Array<FoodField>;
}

export interface FoodField {
    fdcId: number;
    description: string;
    scientificName?: string;
    commonNames?: string;
    additionalDescriptions?: string;
}

export interface FoodReport extends FoodField {
    foodNutrients: Array<FoodNutrient>;
}

interface FoodNutrient {
    amount?: number;
    id?: number;
    nutrient: Nutrient;
}

interface Nutrient {
    id: number;
    name: string;
    number: string;
    unitName: string;
}
