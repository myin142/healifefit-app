import { async, TestBed } from '@angular/core/testing';
import { FoodApiService } from './food-api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FoodReport, SearchResult } from './food-models';

describe('FoodApiService', () => {

    let apiService: FoodApiService;
    let ctrl: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                FoodApiService,
            ],
        });

        apiService = TestBed.get(FoodApiService);
        ctrl = TestBed.get(HttpTestingController);
    });

    it('should create', () => {
        expect(apiService).toBeTruthy();
    });

    describe('Search', () => {

        it('should perform POST request', () => {
            apiService.search('').subscribe();

            ctrl.expectOne({
                method: 'POST',
            });
        });

        it('should contain body with Foundation and Legacy data type', () => {
            apiService.search('').subscribe();

            const request = ctrl.expectOne({});

            expect(request.request.body).toEqual(jasmine.objectContaining({
                includeDataTypes: jasmine.objectContaining({
                    Foundation: true,
                    'SR Legacy': true,
                }),
            }));
        });

        it('should contain body with search input', () => {
            const search = 'search';
            apiService.search(search).subscribe();

            const request = ctrl.expectOne({});

            expect(request.request.body).toEqual(jasmine.objectContaining({
                generalSearchInput: search,
            }));
        });

        it('should return result of http request', async(() => {
            const result: SearchResult = {
                currentPage: 1,
                foods: [],
                totalHits: 1,
                totalPages: 1,
            };

            apiService.search('').subscribe(x => {
                expect(x).toBe(result);
            });

            const request = ctrl.expectOne({});
            request.flush(result);
        }));

    });

    describe('Get Food', () => {

        it('should perform GET request', () => {
            apiService.getFood(1).subscribe();

            ctrl.expectOne({
                method: 'GET',
            });
        });

        it('should contain foodId in url', () => {
            const foodId = 1;
            apiService.getFood(foodId).subscribe();

            const request = ctrl.expectOne({});

            expect(request.request.url).toContain(`${foodId}`);
        });

        it('should return result of http request', async(() => {
            const result: FoodReport = {
                foodNutrients: [],
                description: '',
                fdcId: 1,
            };

            apiService.getFood(1).subscribe(x => {
                expect(x).toBe(result);
            });

            const request = ctrl.expectOne({});
            request.flush(result);
        }));

    });

    afterEach(() => {
        ctrl.verify();
    });

});
