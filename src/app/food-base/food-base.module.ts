import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { MenuModule } from '../menu/menu.module';
import { FoodBasePage } from './food-base.page';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        MenuModule,
        RouterModule.forChild([
            {path: '', component: FoodBasePage},
            {path: 'search', loadChildren: () => import('./search/search.module').then(m => m.SearchModule)},
        ]),
    ],
    declarations: [FoodBasePage],
    providers: [],
})
export class FoodBaseModule {
}
