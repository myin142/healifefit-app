import { Component } from '@angular/core';
import { MenuItem } from '../menu/menu-list/menu-item.model';

@Component({
    selector: 'app-time-base',
    template: '<app-menu-header title="FoodBase"></app-menu-header>' +
        '<ion-content><app-menu-list [menuItems]="menuItems"></app-menu-list></ion-content>',
})
export class FoodBasePage {

    menuItems: MenuItem[] = [
        {name: 'Search', link: 'search'},
    ];

}
