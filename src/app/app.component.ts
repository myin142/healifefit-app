import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { build } from '../environments/build-info';
import { MenuItem } from './menu/menu-list/menu-item.model';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
})
export class AppComponent {

    public build = build;

    public appPages: MenuItem[] = [
        {
            name: 'Cali Base',
            link: '/calibase',
            icon: 'fitness',
        },
        {
            name: 'Food Base',
            link: '/foodbase',
            icon: 'nutrition',
        },
        {
            name: 'Time Base',
            link: '/timebase',
            icon: 'timer',
        },
    ];

    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleLightContent();
            this.splashScreen.hide();
        });
    }
}
