import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PlannerService } from '../planner.service';
import { ModalController } from '@ionic/angular';
import { PlannerItem } from '../planner-item.model';

@Component({
    selector: 'app-planner-add-item',
    templateUrl: './planner-add-item.page.html',
})
export class PlannerAddItemPage implements OnInit {

    @Input() index: number;
    @Input() data: PlannerItem;

    addForm: FormGroup;

    constructor(private builder: FormBuilder,
                private service: PlannerService,
                private modal: ModalController) {}

    ngOnInit(): void {
        this.addForm = this.builder.group({
            name: [this.getValueOrEmpty('name'), Validators.required],
            description: this.getValueOrEmpty('description'),
        });
    }

    private getValueOrEmpty(key: string): string {
        return this.data == null ? '' : this.data[key];
    }

    onSubmit(): void {
        if (this.addForm.invalid) {
            return;
        }

        const values = this.addForm.getRawValue();

        if (this.index !== undefined) {
            this.service.editItem(this.index, values);
        } else {
            this.service.addItem(values);
        }

        this.modal.dismiss();
    }
}
