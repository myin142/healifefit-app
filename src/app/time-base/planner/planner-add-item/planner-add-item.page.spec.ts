import { PlannerAddItemPage } from './planner-add-item.page';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PlannerService } from '../planner.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicModule, ModalController } from '@ionic/angular';

describe('PlannerAddItemPage', () => {

    let page: PlannerAddItemPage;
    let fixture: ComponentFixture<PlannerAddItemPage>;

    let modalSpy: ModalController;
    const serviceSpy: PlannerService = jasmine.createSpyObj('PlannerService', [
        'addItem',
        'editItem',
    ]);

    const validValues = {
        name: 'Name',
        description: 'Text',
    };

    const emptyValues = {
        name: '',
        description: '',
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                IonicModule,
                ReactiveFormsModule,
            ],
            providers: [
                { provide: PlannerService, useValue: serviceSpy },
            ],
            declarations: [ PlannerAddItemPage ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
        });

        fixture = TestBed.createComponent(PlannerAddItemPage);
        page = fixture.componentInstance;

        modalSpy = TestBed.get(ModalController);
        spyOn(modalSpy, 'dismiss');
    });

    it('should create', () => {
        expect(page).toBeTruthy();
    });

    describe('Initialization', () => {

        it('should initialize with empty values if no data defined', () => {
            page.ngOnInit();

            expect(page.addForm.getRawValue()).toEqual(emptyValues);
        });

        it('should initialize with data values if defined', () => {
            page.data = validValues;
            page.ngOnInit();

            expect(page.addForm.getRawValue()).toEqual(validValues);
        });

    });

    describe('Submit', () => {

        beforeEach(() => {
            page.ngOnInit();
        });

        it('should not submit when form invalid', () => {
            setInvalidForm();
            expect(page.addForm.invalid).toBeTruthy();

            page.onSubmit();
            expect(serviceSpy.addItem).not.toHaveBeenCalled();
            expect(modalSpy.dismiss).not.toHaveBeenCalled();
        });

        it('should add item to planner list when index undefined', () => {
            page.index = undefined;
            setValidForm();

            page.onSubmit();
            expect(serviceSpy.addItem).toHaveBeenCalledWith(validValues);
        });

        it('should edit item in planner list when index defined', () => {
            const index = 1;
            page.index = index;
            setValidForm();

            page.onSubmit();
            expect(serviceSpy.editItem).toHaveBeenCalledWith(index, validValues);
        });

        it('should navigate one page back on submit', () => {
            setValidForm();

            page.onSubmit();
            expect(modalSpy.dismiss).toHaveBeenCalled();
        });

        function setInvalidForm() {
            page.addForm.setValue(emptyValues);
        }

        function setValidForm() {
            page.addForm.setValue(validValues);
        }

    });

});
