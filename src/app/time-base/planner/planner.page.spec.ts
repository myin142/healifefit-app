import { PlannerPage } from './planner.page';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PlannerService } from './planner.service';
import { BehaviorSubject } from 'rxjs';
import { IonicModule, ModalController } from '@ionic/angular';
import { PlannerAddItemPage } from './planner-add-item/planner-add-item.page';
import { PlannerItem } from './planner-item.model';

describe('PlannerPage', () => {

    let page: PlannerPage;
    let fixture: ComponentFixture<PlannerPage>;
    let modal: ModalController;

    const service: PlannerService = jasmine.createSpyObj('PlannerService', [
        'savedList', 'saveList', 'deleteItem',
    ]);
    service.savedList = new BehaviorSubject([]);

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ IonicModule ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
            declarations: [ PlannerPage ],
            providers: [
                { provide: PlannerService, useValue: service },
            ],
        }).compileComponents();

        fixture = TestBed.createComponent(PlannerPage);
        page = fixture.componentInstance;

        modal = TestBed.get(ModalController);
    });

    it('should create', () => {
        expect(page).toBeTruthy();
    });

    describe('PlannerList Update', () => {

        beforeEach(() => {
            page.ngOnInit();
        });

        it('should load data from service', () => {
            const list = [{ name: 'Item 1', description: 'Test 1' }];
            service.savedList.next(list);

            expect(page.planItems).toEqual(list);
        });

        it('should disable edit mode if no result', () => {
            service.savedList.next([]);

            expect(page.editEnable).toBeFalsy();
        });

    });

    describe('Open Edit Page', () => {

        beforeEach(() => {
            spyOn(modal, 'create').and.returnValue({
                present: () => {},
            });
        });

        it('should open PlannerAddItem page as modal', async(() => {
            page.openEditPage();

            expect(modal.create).toHaveBeenCalledWith(jasmine.objectContaining({
                component: PlannerAddItemPage,
            }));
        }));

        it('should pass data and index to modal', async(() => {
            const data: PlannerItem = {
                name: 'Data',
                description: 'Text',
            };
            const index = 0;
            page.openEditPage(data, index);

            expect(modal.create).toHaveBeenCalledWith(jasmine.objectContaining({
                componentProps: { data, index },
            }));
        }));

    });

    describe('Close Edit', () => {

        it('should stay disabled when not enabled', () => {
            page.closeEdit(new Event('click'));

            expect(page.editEnable).toBeFalsy();
        });

        it('should disable when enabled', () => {
            page.editEnable = true;
            page.closeEdit(new Event('click'));

            expect(page.editEnable).toBeFalsy();
        });

    });

    describe('Delete Item', () => {

        it('should call delete item with index', () => {
            const index = 0;
            page.deleteItem(index);

            expect(service.deleteItem).toHaveBeenCalledWith(index);
        });

    });

    describe('Reorder List', () => {

        it('should save planner list', () => {
            page.reorderList({ detail: { complete: () => {}}});
            expect(service.saveList).toHaveBeenCalledWith(page.planItems);
        });

    });

});
