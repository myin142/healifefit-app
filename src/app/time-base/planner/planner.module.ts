import { PlannerService } from './planner.service';
import { NgModule } from '@angular/core';
import { PlannerPage } from './planner.page';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { PlannerAddItemPage } from './planner-add-item/planner-add-item.page';
import { ReactiveFormsModule } from '@angular/forms';
import { MenuModule } from '../../menu/menu.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: '', component: PlannerPage },
        ]),
        MenuModule,
    ],
    declarations: [
        PlannerPage,
        PlannerAddItemPage,
    ],
    entryComponents: [
        PlannerAddItemPage,
    ],
    providers: [
        PlannerService,
    ],
})
export class PlannerModule {

}
