import { Component, OnInit } from '@angular/core';
import { PlannerItem } from './planner-item.model';
import { PlannerService } from './planner.service';
import { ModalController } from '@ionic/angular';
import { PlannerAddItemPage } from './planner-add-item/planner-add-item.page';

@Component({
    selector: 'app-planner',
    templateUrl: './planner.page.html',
})
export class PlannerPage implements OnInit {

    editEnable = false;
    planItems: PlannerItem[] = [];

    constructor(private service: PlannerService,
                private modalCtrl: ModalController) {}

    ngOnInit(): void {
        this.service.savedList.subscribe(result => {
            this.planItems = result;

            if (result.length === 0) {
                this.editEnable = false;
            }
        });
    }

    async openEditPage(data: PlannerItem = null, index?: number) {
        const modal = await this.modalCtrl.create({
            component: PlannerAddItemPage,
            componentProps: {
                data,
                index,
            },
        });

        return await modal.present();
    }

    closeEdit(event: Event): void {
        if (!this.editEnable) { return; }

        event.stopPropagation();

        this.editEnable = false;
    }

    deleteItem(index: number): void {
        this.service.deleteItem(index);
    }

    reorderList(event): void {
        event.detail.complete(this.planItems);
        this.service.saveList(this.planItems);
    }

}
