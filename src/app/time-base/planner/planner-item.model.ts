export interface PlannerItem {
    name: string;
    description?: string;
}
