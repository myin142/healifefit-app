import { PlannerService } from './planner.service';
import { TestBed } from '@angular/core/testing';
import { StorageMockProvider } from '../../test/native-storage.mock';

describe('PlannerService', () => {

    let service: PlannerService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                PlannerService,
                StorageMockProvider,
            ],
        });

        service = TestBed.get(PlannerService);
    });

    it('should create', () => {
        expect(service).toBeTruthy();
    });

});
