import { Injectable } from '@angular/core';
import { PlannerItem } from './planner-item.model';
import { CrudStorageService } from '../../service/crud-storage.service';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable()
export class PlannerService extends CrudStorageService<PlannerItem> {

    public savedList = new BehaviorSubject<PlannerItem[]>([]);

    constructor(storage: Storage) {
        super(storage, 'planner-list-key');
    }

}
