import { Component } from '@angular/core';
import { MenuItem } from '../menu/menu-list/menu-item.model';

@Component({
    selector: 'app-time-base',
    template: '<app-menu-header title="TimeBase"></app-menu-header>' +
        '<ion-content><app-menu-list [menuItems]="menuItems"></app-menu-list></ion-content>',
})
export class TimeBasePage {

    menuItems: MenuItem[] = [
        {name: 'Planner', link: 'planner'},
    ];

}
