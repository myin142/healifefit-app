import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { TimeBasePage } from './time-base.page';
import { MenuModule } from '../menu/menu.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        MenuModule,
        RouterModule.forChild([
            {path: '', component: TimeBasePage},
            {path: 'planner', loadChildren: () => import('./planner/planner.module').then(m => m.PlannerModule)},
        ]),
    ],
    declarations: [TimeBasePage],
    providers: [],
})
export class TimeBaseModule {
}
