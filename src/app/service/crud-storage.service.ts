import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

export abstract class CrudStorageService<T> {

    public abstract savedList: BehaviorSubject<T[]>;

    protected constructor(private storage: Storage,
                          private storageKey: string) {
        this.loadInitial();
    }

    private async loadInitial() {
        const initialValue = await this.storage.get(this.storageKey);

        if (initialValue != null) {
            this.savedList.next(initialValue);
        }
    }

    async saveList(list: T[]) {
        await this.storage.set(this.storageKey, list);
        this.savedList.next(list);
    }

    addItem(item: T) {
        const list = this.savedList.getValue();
        list.push(item);
        this.saveList(list);
    }

    deleteItem(index: number): void {
        const list = this.savedList.getValue();
        list.splice(index, 1);
        this.saveList(list);
    }

    editItem(index: number, data: T): void {
        const list = this.savedList.getValue();
        list[index] = data;
        this.saveList(list);
    }

}
