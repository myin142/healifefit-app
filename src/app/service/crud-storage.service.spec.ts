import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import arrayContaining = jasmine.arrayContaining;
import { CrudStorageService } from './crud-storage.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StorageMockProvider } from '../test/native-storage.mock';
import { Storage } from '@ionic/storage';

@Injectable()
class StorageService extends CrudStorageService<any> {
    savedList = new BehaviorSubject([]);

    constructor(storage: Storage) {
        super(storage, 'key');
    }
}

describe('CrudStorageService', () => {

    let service: StorageService;
    let storage: Storage;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                StorageService,
                StorageMockProvider,
            ],
        });

        service = TestBed.get(StorageService);
        storage = TestBed.get(Storage);
    });

    it('should create', () => {
        expect(service).toBeTruthy();
    });

    describe('Load Initial', () => {

        it('should initialize with storage value', fakeAsync(() => {
            const list = ['Item 1'];
            spyOn(storage, 'get').and.returnValue(Promise.resolve(list));

            const storageService: StorageService = new StorageService(storage);
            tick();

            expect(storageService.savedList.getValue()).toEqual(list);
        }));

        it('should not save list when nothing saved', fakeAsync(() => {
            spyOn(storage, 'get').and.returnValue(null);

            const storageService: StorageService = new StorageService(storage);
            tick();

            expect(storageService.savedList.getValue()).toEqual([]);
        }));

    });

    describe('Save List', () => {

        const list: Array<string> = ['Item 1', 'Item 2'];

        beforeEach(() => {
            spyOn(storage, 'set').and.returnValue(Promise.resolve());
        });

        it('should save list', () => {
            service.saveList(list);
            expect(storage.set).toHaveBeenCalledWith(jasmine.any(String), list);
        });

        it('should notify list observer', fakeAsync(() => {
            spyOn(service.savedList, 'next');

            service.saveList(list);
            tick();

            expect(service.savedList.next).toHaveBeenCalledWith(list);
        }));

    });

    describe('Add List Item', () => {

        const item = 'Item';

        it('should add item', () => {
            service.addItem(item);
            expect(service.savedList.getValue()).toContain(item);
        });

        it('should have more items than before', () => {
            const prevLength = service.savedList.value.length;

            service.addItem(item);
            expect(service.savedList.value.length).toBeGreaterThan(prevLength);
        });

        saveListAfterAction(() => {
            service.addItem(item);
            return [item];
        });

    });

    describe('Delete List Item', () => {

        const item = 'Item';
        const list: Array<string> = [item, 'Item 2'];

        beforeEach(() => {
            spyOn(service.savedList, 'getValue').and.returnValue(list);
        });

        it('should delete item by index', () => {
            service.deleteItem(0);
            expect(service.savedList.value).not.toContain(item);
        });

        it('should have less item than before', () => {
            const prevLength = service.savedList.value.length;

            service.deleteItem(0);
            expect(service.savedList.value.length).toBeLessThan(prevLength);
        });

        saveListAfterAction(() => {
            service.deleteItem(0);
            return list.filter(x => x !== item);
        });

    });

    describe('Edit List Item', () => {

        const item = 'Item';
        const list: Array<string> = [item, 'Item 2'];

        beforeEach(() => {
            spyOn(service.savedList, 'getValue').and.returnValue(list);
        });

        it('should edit item by index', () => {
            const index = 1;
            service.editItem(index, item);
            expect(service.savedList.value[index]).toEqual(item);
        });

        it('should have the same length as before', () => {
            const prevLength = service.savedList.value.length;

            service.editItem(1, item);
            expect(service.savedList.value.length).toEqual(prevLength);
        });

        saveListAfterAction(() => {
            const newItem = Object.assign({}, item, {name: 'Item 3'});
            service.editItem(1, newItem);

            return [newItem];
        });

    });

    function saveListAfterAction(action: () => Array<any>): void {
        it('should save list', () => {
            spyOn(service, 'saveList');

            const expectedSave = action();
            expect(service.saveList).toHaveBeenCalledWith(arrayContaining(expectedSave));
        });
    }

});
