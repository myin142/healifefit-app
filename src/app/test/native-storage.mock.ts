import { Storage } from '@ionic/storage';

export class StorageMock {
    private data = {};

    get(key: string): Promise<any> {
        return Promise.resolve(this.data[key]);
    }

    set(key: string, item: any): Promise<any> {
        this.data[key] = item;
        return Promise.resolve();
    }
}

export const StorageMockProvider = { provide: Storage, useClass: StorageMock };
