import { ModalController } from '@ionic/angular';

class ModalControllerMock {
    create() {
        return { present: () => {} };
    }
}

export const ModalControllerMockProvider = { provide: ModalController, useClass: ModalControllerMock };
